# translatingRefFoam

SOLVER: translatingRefFoam

DESCRIPTION: A modified pimpleFoam solver designed to implement a heaving reference frame to the flow field. Built as an alternative to expensive mesh motion techniques. Requires use of groovyBC to compensate for the differential reference frame velocity.

CHANGELOG:

2/6/2019

Initial commit

2/7/2019

Pushed updated solver source files into src/  
Removed FSI code  
Removed unneed solver files  
Updated header on translatingRefFoam.C  
Added comments on all modified code  
Improved code readability  

2/11/2019

Added minimum working example run directory  
Patched singularity issue for c-meshes  
